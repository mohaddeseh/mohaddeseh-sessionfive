package com.example.mohammad.sessiontwo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class DatabasefromActivity extends AppCompatActivity {

    EditText name;
    EditText family;
    EditText number;
    TextView showResults;
    Button showBtn;
    Button show;
    DatabaseHandler databaseHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_databasefrom);

        name = (EditText) findViewById(R.id.name);
        family = (EditText) findViewById(R.id.family);
        number = (EditText) findViewById(R.id.number);
        showResults = (TextView) findViewById(R.id.showResults);
        show = (Button) findViewById(R.id.show);
        showBtn = (Button) findViewById(R.id.showbtn);


        databaseHandler = new DatabaseHandler(
                this, "sessiontwo.db", null, 1


        );
//        insert codes

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                databaseHandler.insertStudent(name.getText().toString(),

                        family.getText().toString(),
                        number.getText().toString()
                );


                name.setText("");
                family.setText("");
                number.setText("");
                Toast.makeText(DatabasefromActivity.this, "data has been saved", Toast.LENGTH_SHORT).show();


            }
        });


//        get reults
        showBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String result = databaseHandler.getStudents();
                showResults.setText(result);
            }
        });

    }
}
