package com.example.mohammad.sessiontwo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class SugarDbActivity extends AppCompatActivity {


    EditText name;
    EditText family;
    EditText number;
    TextView showResults;
    Button showBtn;
    Button show;
    DatabaseHandler databaseHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sugar_db);
        name = (EditText) findViewById(R.id.name);
        family = (EditText) findViewById(R.id.family);
        number = (EditText) findViewById(R.id.number);
        showResults = (TextView) findViewById(R.id.showResults);
        show = (Button) findViewById(R.id.show);
        showBtn = (Button) findViewById(R.id.showbtn);
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StudentModel model = new StudentModel();
                model.setName(name.getText().toString());
                model.setFamily(family.getText().toString());
                model.setNumber(number.getText().toString());
                model.save();
                name.setText("");
                family.setText("");
                number.setText("");
                Toast.makeText(SugarDbActivity.this, "data has been saved", Toast.LENGTH_SHORT).show();
            }
        });

        showBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<StudentModel> students = StudentModel.listAll(StudentModel.class);
                String results = "";

                for (StudentModel student : students) {
                    results += student.getName() + " "
                            + student.getFamily() + " "
                            + student.getNumber() + "\n";
                }
                showResults.setText(results);
            }



        });

    }


}
