package com.example.mohammad.sessiontwo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;
import android.widget.ListView;

import com.example.mohammad.sessiontwo.adapters.ChanelsAdapter;
import com.example.mohammad.sessiontwo.models.ChanelModels;

import java.util.ArrayList;
import java.util.List;

public class ChanelListActivity extends AppCompatActivity {

    ListView ChanelList;
    GridView myGrid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chanel_list);

        ChanelList = (ListView) findViewById(R.id.ChanelList);
        myGrid = (GridView) findViewById(R.id.myGrid);
        ChanelModels Chanel1 = new ChanelModels("chanel 1", "http://tv.irib.ir/files/iribtvnew/mousehover/m01.png", 1);
        ChanelModels Chanel2 = new ChanelModels("chanel 2", "http://tv.irib.ir/files/iribtvnew/mousehover/m02.png", 2);
        ChanelModels Chanel3 = new ChanelModels("chanel 3", "http://tv.irib.ir/files/iribtvnew/mousehover/m03.png", 3);
        ChanelModels Chanel4 = new ChanelModels("chanel 4", "http://tv.irib.ir/files/iribtvnew/mousehover/m04.png", 4);
        ChanelModels Chanel5 = new ChanelModels("chanel 5", "http://tv.irib.ir/files/iribtvnew/mousehover/m05.png", 5);


        List<ChanelModels> chanels = new ArrayList<>();
        chanels.add(Chanel1);
        chanels.add(Chanel2);
        chanels.add(Chanel3);
        chanels.add(Chanel4);
        chanels.add(Chanel5);


        ChanelsAdapter AdapterGrid = new ChanelsAdapter(this, chanels, false);
        myGrid.setAdapter(AdapterGrid);
        ChanelsAdapter AdapterList = new ChanelsAdapter(this, chanels, true);
        ChanelList.setAdapter(AdapterList);

    }
}
