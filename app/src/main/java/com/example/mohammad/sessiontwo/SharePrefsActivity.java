package com.example.mohammad.sessiontwo;

import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

public class SharePrefsActivity extends AppCompatActivity {


    EditText name;
    EditText family;
    EditText age;
    ImageView pageImage;


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_prefs);
        pageImage=(ImageView)findViewById(R.id.pageImage) ;

        Picasso.with(this).load("http://irib.ir/assets/news_images/20170610150641_5349.jpg").into(pageImage);





        name = (EditText) findViewById(R.id.name);
        family = (EditText) findViewById(R.id.family);
        age = (EditText) findViewById(R.id.age);
        name.setText(getshare("name", ""));
        family.setText(getshare("family", ""));
        age.setText(getshare("age", ""));

        findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameValue = name.getText().toString();
                String familyValue = family.getText().toString();
                String ageValue = age.getText().toString();
                saveShared("name", nameValue);
                saveShared("family", familyValue);
                saveShared("age", ageValue);
                showToast("data has been saved!");

                name.setText("");
                family.setText("");
                age.setText("");
            }
        });

    }

    public void showToast(String message) {
        Toast.makeText(this, "data has been saved!", Toast.LENGTH_SHORT).show();

    }

    private void saveShared(String Key, String Value) {


        PreferenceManager.getDefaultSharedPreferences(SharePrefsActivity.this)
                .edit().putString(Key, Value).commit();

    }


    private String getshare(String Key, String defultValue) {
        return PreferenceManager.getDefaultSharedPreferences(SharePrefsActivity.this)
                .getString(Key, defultValue);
    }
}
