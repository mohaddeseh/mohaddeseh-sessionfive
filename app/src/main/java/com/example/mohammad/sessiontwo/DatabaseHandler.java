package com.example.mohammad.sessiontwo;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Mohammad on 6/24/2017.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    String StudentTBL = "" +
            "CREATE TABLE students(" +
            "_ID INTEGER AUTO INCREMENT PRIMARY KEY ," +
            "name TEXT," +
            "family TEXT," +
            "number TEXT" +
            ")";

    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {


        sqLiteDatabase.execSQL(StudentTBL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void insertStudent(String name, String family, String number) {
        String insertQuery = "" +
                "INSERT INTO students ( name , family , number )" +
                " VALUES ( '" + name + "' , '" + family + "','" + number + "' )";

        SQLiteDatabase db = this.getWritableDatabase();

        try{
            db.execSQL(insertQuery);
        }

            catch(Exception e){}




        db.close();
    }

    public String getStudents() {
        String results = "";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT name,family,number from students ", null);
        while (cursor.moveToNext()) {
            results +=
                    cursor.getString(0) +

                            " " +
                            cursor.getString(1) +
                            " " +
                            cursor.getString(2) +
                            " " +


                            "\n";
        }


        db.close();
        return results;
    }
}
