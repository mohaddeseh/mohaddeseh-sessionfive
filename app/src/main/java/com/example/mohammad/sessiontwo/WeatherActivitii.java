package com.example.mohammad.sessiontwo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mohammad.sessiontwo.models.WeatherModel;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.impl.execchain.RequestAbortedException;

public class WeatherActivitii extends AppCompatActivity {

    TextView temperatureView;
    TextView timeView;
    TextView cityNmeView;
    TextView temperatureTesxtView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_activitii);


        cityNmeView = (TextView) findViewById(R.id.cityNameView);
        timeView = (TextView) findViewById(R.id.timeView);
        temperatureView = (TextView) findViewById(R.id.temperatureView);
        temperatureTesxtView = (TextView) findViewById(R.id.temperatureTesxtView);
        findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                getWeather();
                getwWatherByAsync();

            }
        });

    }

    void getwWatherByAsync() {
        String url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22nome%2C%20ak%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.add("name", "jafar");
        params.add("family", "jafari");
        params.add("mobile", "09126655984");
        params.add("email", "jafar@yahoo.com");
        params.add("city", "tehran");
        client.get(url,params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                Toast.makeText(WeatherActivitii.this, "error in connecting", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                parseServerResponse(responseString);


                parseByGson(responseString);
            }
        });

    }

    void getWeather()

    {


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL obj = new URL("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22nome%2C%20ak%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");
                    HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                    con.setRequestMethod("GET");
                    con.setRequestProperty("User-Agent", "Mozilla/5.0");
                    int responseCode = con.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String inputLine;
                        StringBuffer response = new StringBuffer();
                        while ((inputLine = in.readLine()) != null) {
                            response.append(inputLine);
                        }

                        parseServerResponse(response.toString());

                    }


                } catch (Exception e) {
                    Toast.makeText(WeatherActivitii.this, "Error in connecting to website", Toast.LENGTH_SHORT).show();
                }


            }
        }).start();
        ;
    }


    void parseByGson(String response) {


        Gson gson = new Gson();
        WeatherModel weather =
                gson.fromJson(response, WeatherModel.class);
        temperatureView.setText(weather.getQuery().getResults().getChannel().getItem().getCondition().getTemp());
        timeView.setText(weather.getQuery().getResults().getChannel().getItem().getCondition().getDate());
        cityNmeView.setText(weather.getQuery().getResults().getChannel().getLocation().getCity());
        temperatureTesxtView.setText(weather.getQuery().getResults().getChannel().getItem().getCondition().getText());

    }

    void parseServerResponse(String response) {
        Log.d("webservice_debug", response);

        try {
            JSONObject allobj = new JSONObject(response);
            String queryStr = allobj.getString("query");
            JSONObject queryobj = new JSONObject(queryStr);

            String resultsStr = queryobj.getString("results");

            JSONObject resultsobj = new JSONObject(resultsStr);

            String ChannelStr = resultsobj.getString("channel");

            JSONObject channelobj = new JSONObject(ChannelStr);
            String itemStr = channelobj.getString("item");
            JSONObject itemobj = new JSONObject(itemStr);


            String conditionStr = itemobj.getString("condition");

            JSONObject conditionobj = new JSONObject(conditionStr);

            final String date = conditionobj.getString("date");

            final String text = conditionobj.getString("text");
            final String temp = conditionobj.getString("temp");
            Log.d("temp", temp);
            String locationStr = channelobj.getString("location");
            JSONObject locationobj = new JSONObject(locationStr);
            final String city = locationobj.getString("city");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    temperatureView.setText(temp);
                    timeView.setText(date);
                    cityNmeView.setText(city);
                    temperatureTesxtView.setText(text);

                }
            });


        } catch (JSONException e) {
//            Log.d("error_weather webservice_debug" ,e+"");
        }
    }
}
