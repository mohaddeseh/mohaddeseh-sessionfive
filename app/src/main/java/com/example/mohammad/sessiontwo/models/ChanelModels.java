package com.example.mohammad.sessiontwo.models;

/**
 * Created by Mohammad on 6/12/2017.
 */

public class ChanelModels {



    String chanelName;
    String chanelLogoURL ;
    int id;

    public ChanelModels(String chanelName, String chanelLogoURL, int id) {
        this.chanelName = chanelName;
        this.chanelLogoURL = chanelLogoURL;
        this.id = id;
    }

    public String getChanelName() {
        return chanelName;
    }

    public void setChanelName(String chanelName) {
        this.chanelName = chanelName;
    }

    public String getChanelLogoURL() {
        return chanelLogoURL;
    }

    public void setChanelLogoURL(String chanelLogoURL) {
        this.chanelLogoURL = chanelLogoURL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
