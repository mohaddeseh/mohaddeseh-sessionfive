package com.example.mohammad.sessiontwo;

import com.orm.SugarRecord;

/**
 * Created by Mohammad on 6/27/2017.
 */

public class StudentModel extends SugarRecord {
    String name;
    String family;
    String number;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public StudentModel() {
    }

    public StudentModel(String name, String family, String number) {
        this.name = name;
        this.family = family;
        this.number = number;

    }
}
