package com.example.mohammad.sessiontwo.adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.mohammad.sessiontwo.R;

/**
 * Created by Mohammad on 6/12/2017.
 */

public class StudentListAdapter extends BaseAdapter {

    Context mContext;
    String names[];

    public StudentListAdapter(Context mContext, String[] names) {
        this.mContext = mContext;
        this.names = names;
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int position) {
        return names[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View rowView = LayoutInflater.from(mContext).inflate(R.layout.student_list_item, viewGroup, false);

        TextView name =(TextView)rowView.findViewById(R.id.name);
        name.setText(names[position]);

        return rowView;
    }
}
